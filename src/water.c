//author: stanner
//Some code/ideas borrowed from http://pages.cs.wisc.edu/~travitch/pthreads_primer.html
#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>



#define ROUNDS 100
#define BARRS 3

//Func defs
void bond();
void * oxygen_entry_point(void *);
void * hydrogen_entry_point(void *);

//Counters need to be protected by a mutex
int oxygen = 0;
int hydrogen = 0;
pthread_mutex_t mutex;
pthread_mutex_t mutex2;
pthread_barrier_t barrier;
sem_t oxyQueue;
sem_t hydroQueue;

int bonds = 0;


//Driver
int main ( int argc, char *argv[] ) {
	//Init semaphores
	pthread_mutex_init(&mutex, NULL);
	sem_init(&oxyQueue, 0, 0);
	sem_init(&hydroQueue, 0, 0);
	pthread_barrier_init(&barrier, NULL, BARRS);

	int N = 30;	//Threads?

	int k = 0;
	for(; k < ROUNDS; k++) {
		int i = 0;
		pthread_t threads[N];
		for(; i < N; i++) {
//			pthread_t thr;
			int flag = i % 3;
			if (flag == 0) {
				//Spawn O thread

				if(pthread_create(&threads[i], NULL, &oxygen_entry_point, NULL)) {
					printf("Could not create oxygen thread\n");
					pthread_exit(NULL);
				}
			} else {
				//Spawn H thread
				if(pthread_create(&threads[i], NULL, &hydrogen_entry_point, NULL)) {
					printf("Could not create hydrogen thread\n");
					pthread_exit(NULL);
				}


			}


		}

		int j = 0;
		for(; j < N; j++) {
			//Join HOH threads
			if(pthread_join(threads[j], NULL)) {
				printf("Could not join threads\n");
				pthread_exit(NULL);
			}
		}
		printf("%d H20\n", bonds);
	}


	pthread_mutex_destroy(&mutex);
	pthread_exit(NULL);

}

//Oxygen thread stuff
void * oxygen_entry_point(void *arg) {
	pthread_mutex_lock(&mutex);
	oxygen += 1;
	if (hydrogen >= 2) {
		sem_post(&hydroQueue);
		sem_post(&hydroQueue);
		hydrogen -= 2;
		sem_post(&oxyQueue);
		oxygen -= 1;
	} else {
//		token = 0;
		pthread_mutex_unlock(&mutex);
	}

	sem_wait(&oxyQueue);
	bond();
	//Synchronization point
	int race = pthread_barrier_wait(&barrier);
	if(race != 0 && race != PTHREAD_BARRIER_SERIAL_THREAD) {
		printf("Could not wait on barrier (O)\n");
		pthread_exit(NULL);
	}

	int tid = (int)syscall(SYS_gettid);
	if (tid == mutex.__data.__owner) {
		pthread_mutex_unlock(&mutex);
	}

	pthread_exit(NULL);
}

//Hydrogen thread stuff
void * hydrogen_entry_point(void *arg) {
	pthread_mutex_lock(&mutex);
	hydrogen += 1;
	if (hydrogen >= 2 && oxygen >= 1) {
		sem_post(&hydroQueue);
		sem_post(&hydroQueue);
		hydrogen -= 2;
		sem_post(&oxyQueue);
		oxygen -= 1;
	} else {
		pthread_mutex_unlock(&mutex);
	}

	sem_wait(&hydroQueue);
	int race = pthread_barrier_wait(&barrier);
	if(race != 0 && race != PTHREAD_BARRIER_SERIAL_THREAD) {
		printf("Could not wait on barrier (H)\n");
		pthread_exit(NULL);
	}


	int tid = (int)syscall(SYS_gettid);
	if (tid == mutex.__data.__owner) {
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(NULL);
}

//Bond method
void bond(){
	printf("A H2O molecule is formed\n");
	bonds++;
}

